# Algorithm overview
The ARISE project is building an infrastructure that will identify and monitor all multicellular species in the
Netherlands. See the [ARISE website](https://www.arise-biodiversity.nl/) for more details. The digital species
identification (DSI) team is building services that support the development and deployment of AI algorithms which detect
and identify species from a wide variety of digital media such as sound, images, and radar. The ARISE DSI system
connects datasets, AI algorithms and compute platforms.

This repository contains information about AI algorithms that have been integrated with the ARISE DSI system. It
contains additional details associated with algorithms that can be public and shared. The algorithm configuration files
can be part of the algorithm repository or be included in this repository. For each algorithm, there is a README.md file
with links to the configuration files (see the algorithm links in the Algorithm details section below).

## Algorithm descriptor
The algorithm descriptor contains all the configuration information needed for integrating an algorithm with the ARISE
DSI system. Examples are the Docker container image URL and the ontology URL.

## Ontology configuration
The ontology has a name and items, which describes the prediciton names an algorithm can generate. Each ontology item
can contain sub items and each item may have an optional colour field.
```
{
    "name": "Name of the Ontology",
    "items": {
        "Animalia": {
            "items": {
                "Insecta": {
                    "items": {
                        "everything": {"colour"="#accd00"}
                    }
                }
            }
        }
    }
}
```

## Algorithm details
- [AvesEcho-v1](avesecho-v1/README.md).
- [Cricket cicada detector](cricket-cicada-detector/README.md).
- [Diopsis Inference Server](diopsis/README.md).
- [Geese counter](geese_counter/README.md).
- [Human detection 1](human-detection-1/README.md).
- [MegaDetector](megadetector/README.md).
- [Trapper AI](trapper-ai/README.md).
- [DeepFaune](deepfaune/README.md).

## License
Apache License, Version 2.0.
