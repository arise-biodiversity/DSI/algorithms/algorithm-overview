
# AvesEcho-v1 version v1.3.0
    
- [Source code](https://gitlab.com/arise-biodiversity/DSI/algorithms/avesecho-v1.git).
- [Algorithm descriptor](avesecho_v1_algorithm_descriptor.json).
- [Name ontology](https://gitlab.com/arise-biodiversity/DSI/algorithms/avesecho-v1/-/raw/v1.3.0/inputs/avesecho-v1_ontology.json).
