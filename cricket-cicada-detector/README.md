
# Cricket cicada detector version v1.2.0
    
- [Source code](https://gitlab.com/arise-biodiversity/DSI/algorithms/cricket-cicada-detector-capgemini.git).
- [Algorithm descriptor](cricket-cicada-detector_algorithm_descriptor.json).
- [Name ontology](https://gitlab.com/arise-biodiversity/DSI/algorithms/cricket-cicada-detector-capgemini/-/raw/v1.2.0/cricket-cicada-detector_name_ontology.json).
