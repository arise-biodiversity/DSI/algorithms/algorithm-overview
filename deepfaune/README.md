
# DeepFaune version 0.5.0-1.2.1
    
- [Source code](https://gitlab.com/arise-biodiversity/DSI/algorithms/deepfaune-algorithm.git).
- [Algorithm descriptor](deepfaune_algorithm_descriptor.json).
- [Name ontology](https://gitlab.com/arise-biodiversity/DSI/algorithms/deepfaune-algorithm/-/raw/0.5.0-1.2.1/deepfaune-ontology.json).
