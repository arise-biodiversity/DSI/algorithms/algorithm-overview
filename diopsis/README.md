
# Diopsis Inference Server version inference_server_api_update-git-b32ef1d451f2d8667bb40c78e55419a9d9e774fd-snellius
    
- [Algorithm descriptor](diopsis_algorithm_descriptor.json).
- [Name ontology](https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-overview/-/raw/v1.3.0/diopsis/diopsis_ontology.json).
