
# Geese counter version v1.3.0
    
- [Source code](https://gitlab.com/arise-biodiversity/DSI/algorithms/geese-count-3/geese-counter.git).
- [Algorithm descriptor](geese_counter_algorithm_descriptor.json).
- [Name ontology](https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-overview/-/raw/v1.3.0/geese_counter/geese_counter_ontology.json).
