
# Human detection 1 version v1.1
    
- [Source code](https://gitlab.com/arise-biodiversity/DSI/algorithms/human-detection-1.git).
- [Algorithm descriptor](human_detection_1_algorithm_descriptor.json).
- [Name ontology](https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-overview/-/raw/v1.3.0/human-detection-1/ontology.json).
