
# MegaDetector version v5.1.0
    
- [Source code](https://gitlab.com/arise-biodiversity/DSI/algorithms/megadetector.git).
- [Algorithm descriptor](megadetector_algorithm_descriptor.json).
- [Name ontology](https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-overview/-/raw/v1.3.0/megadetector/ontology.json).
