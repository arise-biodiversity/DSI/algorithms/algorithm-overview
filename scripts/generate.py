#!/usr/bin/env python3
import json
from pathlib import Path
from string import Template

def generate_files(algorithm):
    print(algorithm)

    dest = Path(algorithm['directory'])

    readme_path = dest / "README.md"

    descriptor_path = Path(algorithm["descriptor"])

    descriptor_tmpl_path = (dest / "descriptor.json.tmpl")

    class DescriptorTemplate(Template):
        delimiter = '@@'

    if descriptor_tmpl_path.exists():
        descriptor_tmpl = descriptor_tmpl_path.read_text()
        descriptor_path.write_text(DescriptorTemplate(descriptor_tmpl).substitute(algorithm))

    descriptor = json.loads(descriptor_path.read_text())

    metadata = algorithm | {
        "title": algorithm.get("title") or algorithm["name"],
        "name_ontology": descriptor["name_ontology_uri"],
        "descriptor_filename": descriptor_path.name
    }

    readme_path.write_text(("""
# {title} version {version}
    """ +
    ("""
- [Source code]({source}).""" if metadata.get("source") else ""
     ) +
    """
- [Algorithm descriptor]({descriptor_filename}).
- [Name ontology]({name_ontology}).
""").format(**metadata))

def main():
    data = json.loads(Path("metadata.json").read_text())
    for algorithm in data["algorithms"]:
        generate_files(algorithm | { "parent_version": data["version"] })

if __name__ == '__main__':
    main()
