#!/usr/bin/env python3
import json
import subprocess
from pathlib import Path
import tempfile
import requests
import logging
import sys

log = logging.getLogger(__name__)

logging.basicConfig(level=logging.WARNING)

def verify_files(algorithm, *, git_tag: str = ""):
    log.info(algorithm)

    # # Repository
    with tempfile.TemporaryDirectory() as tmpdir:
        if algorithm.get("source"):
            subprocess.check_call(["git", "clone", "-b", algorithm["version"], "--depth", "1", algorithm["source"], tmpdir])

    # Docker url
    descriptor_path = Path(algorithm["descriptor"])
    descriptor = json.loads(descriptor_path.read_text())

    subprocess.check_call(["docker", "manifest", "inspect", "-v", descriptor["docker"]])

    # Ontology
    if git_tag and requests.get(descriptor["name_ontology_uri"]).status_code != 200:
        log.error(f"Failed to get ontology: {descriptor['name_ontology_uri']}")
        sys.exit(1)

def main():
    git_tag = ''
    if len(sys.argv) >= 2:
        git_tag = sys.argv[1]

    metadata = json.loads(Path("metadata.json").read_text())

    if git_tag and git_tag != metadata["version"]:
        raise Exception(f"Passed {git_tag=} does not match {metadata['version']=}")

    for algorithm in metadata["algorithms"]:
        verify_files(algorithm | { "parent_version": metadata["version"] }, git_tag=git_tag)


if __name__ == '__main__':
    main()
