
# Trapper AI version 0.1.4-v02.2024
    
- [Source code](https://gitlab.com/arise-biodiversity/DSI/algorithms/trapper-ai-algorithm.git).
- [Algorithm descriptor](trapper_ai_algorithm_descriptor.json).
- [Name ontology](https://gitlab.com/arise-biodiversity/DSI/algorithms/trapper-ai-algorithm/-/raw/0.1.4-v02.2024/trapper_ai_ontology.json).
